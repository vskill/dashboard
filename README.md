# Панель управление
Панель управления для взаимодействия с узлом.


# Требования к браузеру:
1. Поддержка woff;
2. Поддержка CSS3+HTML5.

## Версии браузеров:
- IE 9+
- Edge 12+
- Firefox 3.6+
- Chrome 5+
- Safari 5.1+
- Opera 11.5+
- iOS Safari 5+
- Android Browser 4.4+
- Blackberry Browser 7+ 
- Opera Mobile 12+
- Chrome for Android all
- Firefox for Android all
- IE Mobile 10+

# Based on 
https://github.com/gmag11/FSBrowserNG
https://demos.creative-tim.com/paper-dashboard-2-pro/examples/dashboard.html