var getPeriod = 1000;

$(document).ready(function() {
    setTimeout(getValues, 500);
    setInterval(getValues, 1000);
    setTimeout(setButtons, 100);
    setTimeout(getPIDValues, 1500);
});

function setButtons() {
  $("#updateBtn").on('click', function(){ getValues(); });
  $("#setPIDBtn").on('click', function(){ setPID(); });


  $("#pump1on").on('click', function(){ sendServer('/pumps.html?id=0&s=1'); });
  $("#pump1off").on('click', function(){ sendServer('/pumps.html?id=0&s=0'); });
  $("#pump2on").on('click', function(){ sendServer('/pumps.html?id=1&s=1'); });
  $("#pump2off").on('click', function(){ sendServer('/pumps.html?id=1&s=0'); });
  $("#pump3on").on('click', function(){ sendServer('/pumps.html?id=2&s=1'); });
  $("#pump3off").on('click', function(){ sendServer('/pumps.html?id=2&s=0'); });
  $("#pump4on").on('click', function(){ sendServer('/pumps.html?id=3&s=1'); });
  $("#pump4off").on('click', function(){ sendServer('/pumps.html?id=3&s=0'); });
  $("#pump5on").on('click', function(){ sendServer('/pumps.html?id=4&s=1'); });
  $("#pump5off").on('click', function(){ sendServer('/pumps.html?id=4&s=0'); });

  $("#ap1on").on('click', function(){ sendServer('/regulator.html?addp0=1'); });
  $("#ap1off").on('click', function(){ sendServer('/regulator.html?addp0=0'); });
  $("#ap2on").on('click', function(){ sendServer('/regulator.html?addp1=1'); });
  $("#ap2off").on('click', function(){ sendServer('/regulator.html?addp1=0'); });

  $("#gk1on").on('click', function(){ sendServer('/regulator.html?gk0=1'); });
  $("#gk1off").on('click', function(){ sendServer('/regulator.html?gk0=0'); });
  $("#gk2on").on('click', function(){ sendServer('/regulator.html?gk1=1'); });
  $("#gk2off").on('click', function(){ sendServer('/regulator.html?gk1=0'); });


  // PID
  $("#setPIDPHBtn").on('click', function(){ setPID(0); });  // 0 - PH
  $("#setPIDECBtn").on('click', function(){ setPID(1); });  // 1 - EC
  $("#updatePHECBtn").on('click', function(){ setPHEC(); });
  $("#updatePIDValuesBtn").on('click', function(){ getPIDValues(); });
  $("#toggleDebug").change(function () { toggleDebug(+this.checked); });
}

function sendServer(data) {
  request = new XMLHttpRequest();
  request.open("GET", data, true);
  request.send();
  main.showNotificationByCode(4, 'info');
}
function setPID () {
  var server = "/sensors.html?kp=" + document.getElementById('Kp').value * 100 + "&ki=" + document.getElementById('Ki').value * 100 + "&kd=" + document.getElementById('Kd').value * 100;
  request = new XMLHttpRequest();
  request.open("GET", server, true);
  request.send();
  main.showNotificationByCode(3, 'info');
}
function getValues() {
    var xh = new XMLHttpRequest();
    xh.onreadystatechange = function(){
      if (xh.readyState == 4){
        if(xh.status == 200) {
          try {
            var res = JSON.parse(xh.responseText);
            
            var tbl = document.getElementById("jsonTable");
            //Удалить старые данные
            if(tbl.getElementsByTagName("tbody")[0]!=null) tbl.removeChild(tbl.getElementsByTagName("tbody")[0]); 

            var tblBody = tbl.appendChild(document.createElement("tbody"));

            $.each(res, function(index, value){
                var row = document.createElement("tr");
                var cell1 = document.createElement("td");
                var cell2 = document.createElement("td");
                var cell3 = document.createElement("td");
                var cell1Text = document.createTextNode(index);
                var cell2Text = document.createTextNode(value);
                var cell3Text = document.createTextNode(value/1000);
                cell1.appendChild(cell1Text);
                cell2.appendChild(cell2Text);
                cell3.appendChild(cell3Text);
                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                tblBody.appendChild(row);
            });
          
            //tbl.appendChild(tblBody);
          } catch(e) { console.log(e); }
        }
      }
    };
    xh.open("GET", "/debug", true);
    xh.send(null);
}

function getPIDValues() {
  var xh = new XMLHttpRequest();
  xh.onreadystatechange = function(){
    if (xh.readyState == 4){
      if(xh.status == 200) {
        try {
          var res = JSON.parse(xh.responseText);
          
          var tbl = document.getElementById("jsonPIDTable");
          //Удалить старые данные
          if(tbl.getElementsByTagName("tbody")[0]!=null) tbl.removeChild(tbl.getElementsByTagName("tbody")[0]); 

          var tblBody = tbl.appendChild(document.createElement("tbody"));

          $.each(res, function(index, value){
              var row = document.createElement("tr");
              var cell1 = document.createElement("td");
              var cell2 = document.createElement("td");
              var cell1Text = document.createTextNode(index);
              var cell2Text = document.createTextNode(value);
              cell1.appendChild(cell1Text);
              cell2.appendChild(cell2Text);
              row.appendChild(cell1);
              row.appendChild(cell2);
              tblBody.appendChild(row);
          });
          setTimeout(getValues, getPeriod);
          //tbl.appendChild(tblBody);
        } catch(e) { console.log(e); }
      }
    }
  };
  xh.open("GET", "/pid_debug", true);
  xh.send(null);
}

function toggleDebug(toggle) {
  var server = "/pid_debug.html?pidDebug=" + toggle;
  request = new XMLHttpRequest();
  request.open("GET", server, true);
  request.send();
}
function setPID(sensor) {
  var server;
  switch (sensor) {
    case 0:
      server = "/pid_debug.html?sensor=" + sensor +  "&kp=" + $('#KpPH').val() * 100 + "&ki=" + $('#KiPH').val() * 100 + "&kd=" + $('#KdPH').val() * 100;
      break;
    case 1:
      server = "/pid_debug.html?sensor=" + sensor +  "&kp=" + $('#KpEC').val() * 100 + "&ki=" + $('#KiEC').val() * 100 + "&kd=" + $('#KdEC').val() * 100;
      break;
  }
  request = new XMLHttpRequest();
  request.open("GET", server, true);
  request.send();
  main.showNotificationByCode(3, 'info');
}

function setPHEC() {
  var server = "/pid_debug.html?ph=" + document.getElementById('PHcustom').value * 1000 + "&ec=" + document.getElementById('ECcustom').value * 1000;
  request = new XMLHttpRequest();
  request.open("GET", server, true);
  request.send();
  main.showNotificationByCode(3, 'info');
}