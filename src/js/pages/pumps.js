$(document).ready(function() {
    $("#startPump").on('click', function(){
        startPump();
    });
    $("#sendABC").on('click', function(){
        sendABC();
    });
    //$('select').selectpicker();
    $("#pumpTime").inputSpinner({
        beforeButton: "font-size:18px; margin: 0;",
        afterButton: "font-size:18px; margin: 0;",
        buttonsClass: "btn btn-sm btn-primary btn-round",
    });

    $("#additionalP0").change(function () { sendPowerControl(0, +this.checked); });
    $("#additionalP1").change(function () { sendPowerControl(1, +this.checked); });

    setTimeout(loadValues, 500);
    setTimeout(main.appendSelect, 200);
});

function loadValues(){
    var xh = new XMLHttpRequest();
    xh.onreadystatechange = function(){
      if (xh.readyState == 4){
        if(xh.status == 200) {
          try {
            var res = JSON.parse(xh.responseText);
            initSlider(res.A, res.B);
          } catch(e) { console.log(e); }
          } else running = false;
      }
    };
    xh.open("GET", "/abccomp", true);
    xh.send(null);
};
  

function sendPowerControl(code, state) { 
    switch (code) {
        case 0:
            element = "addp0";
            break;
        case 1:
            element = "addp1";
            break;
    }
    var server = "/regulator.html?" + element + "=" + state;
        request = new XMLHttpRequest();
        request.open("GET", server, true);
        request.send();
}


function initSlider(a_value, b_value, c_value) {
    var slider = $("#ABC_Slider").slider({
        id: "ABC_Slider", 
        min: 0, 
        max: 100, 
        range: true, 
        value: [a_value, a_value + b_value] 
    });
    values = slider.slider('getValue');
    $("#A_value").val(values[0]);
    $("#B_value").val(values[1] - values[0]);
    $("#C_value").val(100 - values[1]);
    $("#ABC_Slider").on("change", function(slideEvt) {
        $("#A_value").val(slideEvt.value.newValue[0]);
        $("#B_value").val(slideEvt.value.newValue[1] - slideEvt.value.newValue[0]);
        $("#C_value").val(100 - slideEvt.value.newValue[1]);
    });
}

function sendABC() {
    var server = "/pumps.html?a_comp=" + Number($("#A_value").val()) + "&b_comp=" + Number($("#B_value").val()) + "&c_comp=" + Number($("#C_value").val());
    request = new XMLHttpRequest();
	request.open("GET", server, true);
    request.send();
    main.showNotificationByCode(3, 'info');
}

function startPump() {
    var seconds = Number(document.getElementById('pumpTime').value) * 1000;
    var server = "/pumps.html?id=" + Number(document.getElementById("pumpId").selectedIndex) + "&t=" + seconds;
    request = new XMLHttpRequest();
	request.open("GET", server, true);
    request.send();
    main.showNotificationByCode(4, 'info');
}