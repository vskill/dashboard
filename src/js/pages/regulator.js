var $TABLE = $('#table');
var $BTN = $('#export-btn');
var $EXPORT = $('#export');

function parseRules (jsonData, count) {
    $.each(jsonData, function(key, value){
        if(value.type == 0) {
            addRuleTile(value.active, value.param, value.condition, value.value / 1000, value.target, value.result, value.time, value.otherwise, value.type, key);
        } else if(value.type == 1) {
            addRuleTile(value.active, value.param, value.condition, value.value, value.target, value.result, null, value.otherwise, value.type, key);
        }
    });
}

function loadValues(){
    var xh = new XMLHttpRequest();
    xh.onreadystatechange = function(){
      if (xh.readyState == 4){
        if(xh.status == 200) {
          try {
            var res = JSON.parse(xh.responseText);
            $('#sequenceRules').val(res.sequence / 1000);
            parseRules(res.rules, res.count);
          } catch(e) { console.log(e); }
        }
      }
    };
    xh.open("GET", "/rules", true);
    xh.send(null);
};
  
$(document).ready(function() {
    setTimeout(loadValues, 500);
    setTimeout(updateEditable, 1000);
    setTimeout(bindInputs,1500);
});

function numberTiles() {
    var c = 0;
    $("div.tileRow").each(function(index, el) {
        $(el).attr('id', 'tile' + index);
        $(el).find(".ruleActive").attr('id', 'ruleActive' + index);
        $(el).find(".deleteTileBtn").attr('onclick', 'deleteTile(' + index + ')');
        $(el).find(".selParam").attr('id', 'selParam' + index);
        $(el).find(".selIfElse").attr('id', 'selIfElse' + index);
        $(el).find(".selData").attr('id', 'selData' + index);
        $(el).find(".selResult").attr('id', 'selResult' + index);
        $(el).find(".selResultIs").attr('id', 'selResultIs' + index);
        $(el).find(".selResultOn").attr('id', 'selResultOn' + index);
        $(el).find(".selResultElse").attr('id', 'selResultElse' + index);
        $(el).find(".selgCon").attr('id', 'selgCon' + index);
        $(el).find(".selgData").attr('id', 'selgData' + index);
        $(el).find(".selgConCondition").attr('id', 'selgConCondition' + index);
        $(el).find(".ruleNumber").html(index + 1);
        c++;
    });
    return c;
}


function deleteTile(tilenum) {
    if (confirm("Вы уверены, что хотите удалить правило?")) {
        $('#tile' + tilenum).remove(); 
    }
    numberTiles();
}

function addRuleTile(active, param, condition, value, target, result, time, otherwise, type, rulenum) {
    if(numberTiles() >= 4) {
        alert("В настоящий момент система поддержаивает не более 4-х правил. Удалите одно из старых правил перед созданием нового.");
        return;
    }
    var $gConCol = "<div class='col-md-6 tileRow' id='tile" + rulenum + "'>                                           \
    <div class='card'>																		                      \
        <div class='card-header'>                                                                                 \
          <div class='stats pull-left card-title' style='width: 60%; margin-bottom:0px;'>                         \
              <div class='form-check'>                                                                            \
                  <label class='form-check-label' title='Активность правила'>                                     \
                      <input class='form-check-input ruleActive' id='ruleActive" + rulenum + "' type='checkbox'>  \
                      <span class='form-check-sign'></span>                                                       \
                      <h5 class='card-checbox-form' style='margin-bottom:0px;'>Правило №<span class='ruleNumber'>" + rulenum + "</span> (геркон)\
                      <span id='ruleType" + rulenum + "' style='display:none'>" + type + "</span></h5>\
                  </label>                                                                                        \
              </div>                                                                                              \
          </div>                                                                                                  \
          <button type='submit' onclick='deleteTile(" + rulenum + ")' class='btn btn-danger btn-sm btn-round pull-right deleteTileBtn'>Удалить</button>\
        </div>                                                                                                    \
        <div class='card-body' style='font-size: 1.5em;'>                                                         \
            ЕСЛИ (<b><span class='selgCon' id='selgCon" + rulenum + "'>" + param + "</span>                       \
              <span class='selgConCondition' id='selgConCondition" + rulenum + "'>" + condition + "</span>        \
              <span class='selgData' id='selgData" + rulenum + "'>" + value + "</span></b>                          \
              ) ТО <br>                                                                                           \
            <b><span class='selResult' id='selResult" + rulenum + "'>" + target + "</span></b> =                  \
            <b><span class='selResultIs' id='selResultIs" + rulenum + "'>" + result + "</span></b>                \
            ИНАЧЕ <b><span class='selResultElse' id='selResultElse" + rulenum + "'>" + otherwise + "</span></b>   \
        </div>                                                                                                    \
        <div class='card-footer'>                                                                                 \
            <i>Для редактирования нажмите на выделенный <span class='editable-click'>пунктиром</span> элемент</i> \
        </div>                                                                                                    \
    </div>                                                                                                        \
    </div>"	                                                                                                            
    
    var $sensorsCol = "<div class='col-md-6 tileRow' id='tile" + rulenum + "'>                                           \
    <div class='card'>																		                      \
        <div class='card-header'>                                                                                 \
          <div class='stats pull-left card-title' style='width: 60%; margin-bottom:0px;'>                         \
              <div class='form-check'>                                                                            \
                  <label class='form-check-label' title='Активность правила'>                                     \
                      <input class='form-check-input ruleActive' id='ruleActive" + rulenum + "' type='checkbox'>  \
                      <span class='form-check-sign'></span>                                                       \
                      <h5 class='card-checbox-form' style='margin-bottom:0px;'>Правило №<span class='ruleNumber'>" + rulenum + "</span> (сенсор)\
                      <span id='ruleType" + rulenum + "' style='display:none'>" + type + "</span></h5>\
                  </label>                                                                                        \
              </div>                                                                                              \
          </div>                                                                                                  \
          <button type='submit' onclick='deleteTile(" + rulenum + ")' class='btn btn-danger btn-sm btn-round pull-right deleteTileBtn'>Удалить</button>\
        </div>                                                                                                    \
        <div class='card-body' style='font-size: 1.5em;'>                                                         \
            ЕСЛИ (<b><span class='selParam' id='selParam" + rulenum + "'>" + param + "</span>                     \
              <span class='selIfElse' id='selIfElse" + rulenum + "'>" + condition + "</span>                      \
              <span class='selData' id='selData" + rulenum + "'>" + value + "</span></b>                          \
              ) ТО <br>                                                                                           \
            <b><span class='selResult' id='selResult" + rulenum + "'>" + target + "</span></b> =                  \
            <b><span class='selResultIs' id='selResultIs" + rulenum + "'>" + result + "</span></b> на             \
            <b><span class='selResultOn' id='selResultOn" + rulenum + "'>" + time + "</span></b> сек <br>        \
            ИНАЧЕ <b><span class='selResultElse' id='selResultElse" + rulenum + "'>" + otherwise + "</span></b>   \
        </div>                                                                                                    \
        <div class='card-footer'>                                                                                 \
            <i>Для редактирования нажмите на выделенный <span class='editable-click'>пунктиром</span> элемент</i> \
        </div>                                                                                                    \
    </div>                                                                                                        \
    </div>"	

    // В зависимости от выбранного типа добавляем свой тайл
    if(type === 0)
        $("#rulesContainer").append($sensorsCol);
    else if(type === 1)
        $("#rulesContainer").append($gConCol);
    
    numberTiles();
    updateEditable();
    $('#ruleActive' + rulenum).prop( "checked", !!+active );
    $('#selParam' + rulenum).editable('setValue', param);
    $('#selIfElse' + rulenum).editable('setValue', condition);
    $('#selData' + rulenum).editable('setValue', value);
    $('#selResult' + rulenum).editable('setValue', target);
    $('#selResultIs' + rulenum).editable('setValue', result);
    $('#selResultOn' + rulenum).editable('setValue', time/1000);
    $('#selResultElse' + rulenum).editable('setValue', otherwise);

    $('#selgCon' + rulenum).editable('setValue', param);
    $('#selgData' + rulenum).editable('setValue', value);
    $('#selgConCondition' + rulenum).editable('setValue', condition);
}

function bindInputs() {
    $('#applyRules').click(function() {
        applyRules();
    });
    $('.sensors-tile-add').click(function() {
        addRuleTile(0, 0, 1, 1.5, 0, 0, 0, 2, 0, numberTiles());
    });
    $('.gcon-tile-add').click(function() {
        addRuleTile(0, 0, 0, 1, 0, 0, null, 2, 1, numberTiles());
    });
}

function getConditionNumber(value) {
    switch (value) {
        case '&lt;':
            return 0;
        case '&gt;':
            return 1;
        case '&le;':
            return 2;
        case '&ge;':
            return 3;
        case '=':
            return 4;
    }
}

function getGerconConditionNumber(value) {
    switch (value) {
        case '=':
            return 0;
        case '&ne':
            return 1;
    }
}

function getAdditionalPowerNumber(value) {
    switch (value) {
        case 'Доп. нагрузка 1':
            return 0;
        case 'Доп. нагрузка 2':
            return 1;
    }
}

function getResultNumber(value) {
    switch (value) {
        case 'Выкл':
            return 0;
        case 'Вкл':
            return 1;
        case '-':
            return 2;
    }
}

function getSensorNumber(value) {
    switch (value) {
        case 'PH':
            return 0;
        case 'EC':
            return 1;
    }
}

function getGerconNumber(value) {
    switch (value) {
        case 'Геркон 1':
            return 0;
        case 'Геркон 2':
            return 1;
    }
}

function applyRules() {
    if(numberTiles() == 0) {
        var server = "/regulator.html?count=0"; 
        request = new XMLHttpRequest();
        request.open("GET", server, true);
        request.send();
    } else {
        for(var i = 0; i < numberTiles(); i++) {
            var count = numberTiles();

            var sequence = $("#sequenceRules").val() * 1000;

            var active = +document.getElementById('ruleActive' + i).checked;
            var type = document.getElementById('ruleType' + i).innerHTML;
            
            var condition;
            var value;
            var param;
            var time;

            if(type == 0) {
                condition = getConditionNumber(document.getElementById('selIfElse' + i).innerHTML);
                value = document.getElementById('selData' + i).innerHTML * 1000;
                param = getSensorNumber(document.getElementById('selParam' + i).innerHTML);
                time = document.getElementById('selResultOn' + i).innerHTML * 1000;
            } else if (type == 1) {
                condition = getGerconConditionNumber(document.getElementById('selgConCondition' + i).innerHTML);
                value = getResultNumber(document.getElementById('selgData' + i).innerHTML);
                param = getGerconNumber(document.getElementById('selgCon' + i).innerHTML);
            }
            
            var target = getAdditionalPowerNumber(document.getElementById('selResult' + i).innerHTML);
            var result = getResultNumber(document.getElementById('selResultIs' + i).innerHTML);
            var otherwise = getResultNumber(document.getElementById('selResultElse' + i).innerHTML);

            var server = "/regulator.html?count=" + count + "&sequence=" + sequence + "&rule=" + i + 
                "&type=" + type + "&active=" + active + "&param=" + param +  "&condition="  + condition + 
                "&value=" + value + "&target=" + target + "&result=" + result + 
                "&time=" + time + "&otherwise=" + otherwise;
            
            request = new XMLHttpRequest();
            request.open("GET", server, true);
            request.send();
        }
    }
    main.showNotificationByCode(4, 'info');
}

function updateRemovers() {
    $('.table-remove').off('click').on('click', function(e) {
        if (confirm("Вы уверены, что хотите удалить правило?")) {
            $(this).parents('tr').detach();
        }
        numberRows($("#table"));
    });
}

function updateEditable() {
    //toggle `popup` / `inline` mode
    $.fn.editable.defaults.mode = 'popup';     
    var numConfig = {
      //mode: 'popup',
      placement: 'auto',
      type: 'number',
      step: '0.01',
      min: '0.00',
      max: '1000',
      inputclass: 'table-input',
      emptytext: 'Пусто'
    }

    var onOffConfig = {
        type: 'select',
        container: 'body',
        placement: 'auto',
        value: 1,
        customClass: 'form-control',
        source: [
            {value: 1, text: 'Вкл'},
            {value: 0, text: 'Выкл'}
        ]
        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    }

    var onOffElseConfig = {
        type: 'select',
        container: 'body',
        placement: 'auto',
        value: 1,
        customClass: 'form-control',
        source: [
            {value: 1, text: 'Вкл'},
            {value: 0, text: 'Выкл'},
            {value: 2, text: '-'}
        ]
    }

    $('.selData').editable(numConfig);

    $('.selIfElse').editable({
        type: 'select',
        //title: 'Select status',
        //placement: 'auto',
        container: 'body',
        value: 0,
        customClass: 'form-control',
        source: [
            {value: 0, text: '<'},
            {value: 1, text: '>'},
            {value: 2, text: '≤'}, //&#8804;
            {value: 3, text: '≥'}, //&#8805;
            {value: 4, text: '='}
        ]
    });

    $('.selResultIs').editable(onOffConfig);
    $('.selResultOn').editable(numConfig);
    $('.selResultElse').editable(onOffElseConfig);
    
    $('.selResult').editable({
        type: 'select',
        //title: 'Select status',
        container: 'body',
        placement: 'auto',
        value: 0,
        customClass: 'form-control',
        source: [
            {value: 0, text: 'Доп. нагрузка 1'},
            {value: 1, text: 'Доп. нагрузка 2'}
        ]
    });

    $('.selParam').editable({
        type: 'select',
        //title: 'Select status',
        container: 'body',
        placement: 'auto',
        value: 0,
        customClass: 'form-control',
        source: [
            {value: 0, text: 'PH'},
            {value: 1, text: 'EC'}
        ]
    });

    $('.selgCon').editable({
        type: 'select',
        container: 'body',
        placement: 'auto',
        value: 0,
        customClass: 'form-control',
        source: [
            {value: 0, text: 'Геркон 1'},
            {value: 1, text: 'Геркон 2'}
        ]
    });

    $('.selgConCondition').editable({
        type: 'select',
        container: 'body',
        value: 0,
        customClass: 'form-control',
        source: [
            {value: 0, text: '='},
            {value: 1, text: '≠'}
        ]
    });

    $('.selgData').editable(onOffConfig);

};


// // $('.table-up').click(function () {
// //     var $row = $(this).parents('tr');
// //     if ($row.index() === 1) return; // Don't go above the header
// //     $row.prev().before($row.get(0));
// // });

// // $('.table-down').click(function () {
// //     var $row = $(this).parents('tr');
// //     $row.next().after($row.get(0));
// // });

// // A few jQuery helpers for exporting only
// jQuery.fn.pop = [].pop;
// jQuery.fn.shift = [].shift;

// $BTN.click(function () {
// var $rows = $TABLE.find('tr:not(:hidden)');
// var headers = [];
// var data = [];

// // Get the headers (add special header logic here)
// $($rows.shift()).find('th:not(:empty)').each(function () {
// headers.push($(this).text().toLowerCase());
// });

// // Turn all existing rows into a loopable array
// $rows.each(function () {
// var $td = $(this).find('td');
// var h = {};

// // Use the headers from earlier to name our hash keys
// headers.forEach(function (header, i) {
// h[header] = $td.eq(i).text();
// });

// data.push(h);
// });

// // Output the result
// $EXPORT.text(JSON.stringify(data));
// });
