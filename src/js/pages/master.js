wizard = {
    initWizard: function() {
    //Русская локализация
    $.extend( $.validator.messages,  {
      required: "Это поле необходимо заполнить.",
      remote: "Пожалуйста, введите правильное значение.",
      email: "Пожалуйста, введите корректный адрес электронной почты.",
      url: "Пожалуйста, введите корректный URL.",
      date: "Пожалуйста, введите корректную дату.",
      dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
      number: "Пожалуйста, введите число.",
      digits: "Пожалуйста, вводите только цифры.",
      creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
      equalTo: "Пожалуйста, введите такое же значение ещё раз.",
      extension: "Пожалуйста, выберите файл с правильным расширением.",
      maxlength: $.validator.format( "Пожалуйста, введите не больше {0} символов." ),
      minlength: $.validator.format( "Пожалуйста, введите не меньше {0} символов." ),
      rangelength: $.validator.format( "Пожалуйста, введите значение длиной от {0} до {1} символов." ),
      range: $.validator.format( "Пожалуйста, введите число от {0} до {1}." ),
      max: $.validator.format( "Пожалуйста, введите число, меньшее или равное {0}." ),
      min: $.validator.format( "Пожалуйста, введите число, большее или равное {0}." )
    });
    // Code for the Validator
    var $validator = $('.card-wizard form').validate({
      rules: {
        login1: {
          required: true,
          minlength: 3
        },
        password1: {
          required: true,
          minlength: 3
        }
      },
      highlight: function(element) {
        $(element).closest('.input-group').removeClass('has-success').addClass('has-danger');
      },
      success: function(element) {
        $(element).closest('.input-group').removeClass('has-danger').addClass('has-success');
      }
    });

    // Wizard Initialization
    $('.card-wizard').bootstrapWizard({
      'tabClass': 'nav nav-pills',
      'nextSelector': '.btn-next',
      'previousSelector': '.btn-previous',

      onNext: function(tab, navigation, index) {
        var $valid = $('.card-wizard form').valid();
        if (!$valid) {
          $validator.focusInvalid();
          return false;
        }
      },

      onInit: function(tab, navigation, index) {
        //check number of tabs and fill the entire row
        var $total = navigation.find('li').length;
        var $wizard = navigation.closest('.card-wizard');

        first_li = navigation.find('li:first-child a').html();
        $moving_div = $("<div class='moving-tab'></div>");
        $moving_div.append(first_li);
        $('.card-wizard .wizard-navigation').append($moving_div);



        refreshAnimation($wizard, index);

        $('.moving-tab').css('transition', 'transform 0s');
      },

      onTabClick: function(tab, navigation, index) {
        var $valid = $('.card-wizard form').valid();

        if (!$valid) {
          return false;
        } else {
          return true;
        }
      },

      onTabShow: function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;

        var $wizard = navigation.closest('.card-wizard');

        // If it's the last tab then hide the last button and show the finish instead
        if ($current >= $total) {
          $($wizard).find('.btn-next').hide();
          $($wizard).find('.btn-finish').show();
        } else {
          $($wizard).find('.btn-next').show();
          $($wizard).find('.btn-finish').hide();
        }

        button_text = navigation.find('li:nth-child(' + $current + ') a').html();

        setTimeout(function() {
          $('.moving-tab').html(button_text);
        }, 150);

        var checkbox = $('.footer-checkbox');

        if (!index == 0) {
          $(checkbox).css({
            'opacity': '0',
            'visibility': 'hidden',
            'position': 'absolute'
          });
        } else {
          $(checkbox).css({
            'opacity': '1',
            'visibility': 'visible'
          });
        }

        refreshAnimation($wizard, index);
      }
    });


    // Prepare the preview for profile picture
    $("#wizard-picture").change(function() {
      readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function() {
      wizard = $(this).closest('.card-wizard');
      wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
      $(this).addClass('active');
      $(wizard).find('[type="radio"]').removeAttr('checked');
      $(this).find('[type="radio"]').attr('checked', 'true');
    });

    $('[data-toggle="wizard-checkbox"]').click(function() {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).find('[type="checkbox"]').removeAttr('checked');
      } else {
        $(this).addClass('active');
        $(this).find('[type="checkbox"]').attr('checked', 'true');
      }
    });

    $('.set-full-height').css('height', 'auto');

    //Function to show image before upload

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(window).resize(function() {
      $('.card-wizard').each(function() {
        $wizard = $(this);

        index = $wizard.bootstrapWizard('currentIndex');
        refreshAnimation($wizard, index);

        $('.moving-tab').css({
          'transition': 'transform 0s'
        });
      });
    });

    function refreshAnimation($wizard, index) {
      $total = $wizard.find('.nav li').length;
      $li_width = 100 / $total;

      total_steps = $wizard.find('.nav li').length;
      move_distance = $wizard.width() / total_steps;
      index_temp = index;
      vertical_level = 0;

      mobile_device = $(document).width() < 600 && $total > 3;

      if (mobile_device) {
        move_distance = $wizard.width() / 2;
        index_temp = index % 2;
        $li_width = 50;
      }

      $wizard.find('.nav li').css('width', $li_width + '%');

      step_width = move_distance;
      move_distance = move_distance * index_temp;

      $current = index + 1;

      // if($current == 1 || (mobile_device == true && (index % 2 == 0) )){
      //     move_distance -= 8;
      // } else if($current == total_steps || (mobile_device == true && (index % 2 == 1))){
      //     move_distance += 8;
      // }

      if (mobile_device) {
        vertical_level = parseInt(index / 2);
        vertical_level = vertical_level * 38;
      }

      $wizard.find('.moving-tab').css('width', step_width);
      $('.moving-tab').css({
        'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
        'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

      });
    }
  }
};

$(document).ready(function() {
  setTimeout(getNetwork, 1000);
  setTimeout(bindInputs, 500);
  setInterval(getCalibrationValues, 1000);
});

function setHtmlValuesFromJson (jsonData) {
  $.each(jsonData, function(key, value){
    if (document.getElementById(key) != null) {
      document.getElementById(key).innerHTML=(value/1000).toFixed(2);
    }
  });
}

function parseCalibrations(arr){
  var stateArr = ["calibrationState1", "calibrationState2"];
  for (var i = 0; i < arr.length; i++) {
  var pump = parseInt(arr[i]);
    if(document.getElementById(stateArr[i]) != null) {
      document.getElementById(stateArr[i]).classList.remove('text-success');
      document.getElementById(stateArr[i]).classList.remove('text-danger');
      document.getElementById(stateArr[i]).classList.add(pump ? "text-success" : "text-danger");
    }
  }
}

function getCalibrationValues() {
    var xh = new XMLHttpRequest();
    xh.onreadystatechange = function(){
      if (xh.readyState == 4){
        if(xh.status == 200) {
          try {
            var res = JSON.parse(xh.responseText);
            setHtmlValuesFromJson(res);
            parseCalibrations(res.calibStates);
          } catch(e) { console.log(e); }
          } else running = false;
      }
    };
    xh.open("GET", "/master", true);
    xh.send(null);
}

function bindInputs() {
  $("#sensorCalibration").on('click', function(){ startCalibration(); });
  $("#clearCalibration").on('click', function(){ clearCalibration(); });
  $("#finishWizard").on('click', function(){ finishWiz(); });
  
  main.appendSelect();  // Custom select
  $("input[type='number']").inputSpinner();
  $('#dtPicker').datetimepicker({
    icons: {
    time: "nc-icon icon-clock-1",
    date: "nc-icon icon-calendar",
    up: "nc-icon icon-up-open",
    down: "nc-icon icon-down-open",
    previous: 'nc-icon icon-left-open',
    next: 'nc-icon icon-right-open',
    today: 'nc-icon icon-desktop',
    clear: 'nc-icon icon-trash-empty',
    close: 'nc-icon icon-cancell'
    },
    locale: 'ru',
    format: 'DD.MM.YYYY HH:mm:ss',
}).on('dp.show dp.update', function () {
  $(".datepicker-years .picker-switch").removeAttr('title')
      .on('click', function (e) {
          e.stopPropagation();
      });
  });

  var momentString = moment().locale('ru').format('MM-DD-YYYY HH:mm:ss'); 
  $('#dtPicker').data("DateTimePicker").date(new Date(momentString));
}

function startCalibration() {
  var value = Number(document.getElementById('sensorValue').value * 1000);
  var server = "/sensors.html?sensor=" + Number(document.getElementById("sersorType").selectedIndex) + "&value=" + value;
  request = new XMLHttpRequest();
  request.open("GET", server, true);
  request.send();
  main.showNotificationByCode(3, 'info');
}

function clearCalibration() {
var server = "/sensors.html?clearCalib=" + Number(document.getElementById("sersorType").selectedIndex);
request = new XMLHttpRequest();
request.open("GET", server, true);
request.send();
main.showNotificationByCode(3, 'info');
}

function finishWiz() {
  // Сохранение настроек
  var u_time = moment.utc(document.getElementById('dtPicker').value, 'MM-DD-YYYY HH:mm:ss').unix();
  var server = "/master.html?wwwuser=" + $("#login").val() + 
                            "&wwwpass=" + $("#password").val() +
                            "&wwwauth=" + 1 +
                            "&ssidname=" + $("#ssidname").val() +
                            "&ssidpassword=" + $("#ssidpassword").val() +
                            "&ntpenabled=" + Number(document.getElementById('ntpenabled').checked) + 
                            "&tz=" + document.getElementById('tz').value + 
                            "&u_time=" + u_time;
  request = new XMLHttpRequest();
	request.open("GET", server, true);
  request.send();
  // Уведомление
  var options = {
    from: 'bottom',
    align: 'right',
    timer: 10000,
    message: 'Данные сохранены.<br><b>Пожалуйста, дождитесь перезагрузки устройства.</b>'
  }
  main.showNotification(options);

  setTimeout(function(){
    window.location.href = '/index.html';
 }, 10000);
}

function getNetwork() {
  request = new XMLHttpRequest();
  if (request) {
      request.open("GET", "/scan", true);
      request.addEventListener("load", wifiScan)
      request.send();
  }
}

function securityStr(security) {
  switch(security) {
    case 7:
      return 'Открыта';
    case 5:
      return "WEP <i class='nc-icon icon-lock'></i>";
    case 2:
      return "WPA <i class='nc-icon icon-lock'></i>";
    case 4:
      return "WPA2 <i class='nc-icon icon-lock'></i>";
    case 8:
      return "WPA/WPA2 <i class='nc-icon icon-lock'></i>";
  }
}

function selssid(value) {
  $("#ssidname").val(value);
}

function getSignalPower(rssi) {
  var signal;
  var bars = "<div class='first-bar bar'></div><div class='second-bar bar'></div><div class='third-bar bar'></div><div class='fourth-bar bar'></div><div class='fifth-bar bar'></div>"; 
  if(rssi >= -67) {                    // amazing
    signal = "<div class='signal-bars mt1 sizing-box good five-bars'>";
  }
  else if(rssi < -67 && rssi >= -70) { // good
    signal = "<div class='signal-bars mt1 sizing-box good four-bars'>";
  }
  else if(rssi < -70 && rssi >= -80) { // ok
    signal = "<div class='signal-bars mt1 sizing-box ok three-bars'>";
  }
  else if(rssi < -80 && rssi >= -90) { // not good
    signal = "<div class='signal-bars mt1 sizing-box bad two-bars'>";
  }
  else if(rssi < -90) {               // bad
    signal = "<div class='signal-bars mt1 sizing-box bad one-bar'>";
  }
  return (signal + bars + "</div>");
}

function wifiScan(res) {
  var array;
  if (!res || (res.target.responseText == '[]')) {
      setTimeout(function () { getNetwork(); }, 5000);
      return;
  }
  array = JSON.parse(res.target.responseText);
  array.sort(function (a, b) { return a.rssi - b.rssi });
  array.reverse();
  //document.getElementById("numNets").innerHTML = array.length;
  var table = document.getElementById("networks");
  table.innerHTML = "";
  for (var i = 0; i < array.length; i++) {
      var row = document.createElement("tr");
      row.innerHTML = "<td style='text-align:left;'><a href='javascript:selssid(\"" + array[i].ssid + "\")'>" 
        + array[i].ssid + "</td><td>" 
        + array[i].channel + "</td><td style='text-align:left;'>" 
        + securityStr(array[i].secure) + "</td><td>" 
        + getSignalPower(array[i].rssi) + "</td>";

      table.appendChild(row);
  }
}
