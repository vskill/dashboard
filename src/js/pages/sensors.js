var isMinMaxParsed = false;
var reloadPeriod = 2000;

$(document).ready(function() {
    setTimeout(bindInputs, 50);
    setTimeout(loadValues, 200);
    setTimeout(main.appendSelect, 100);
    //setInterval(loadValues, 1000);
});

function bindInputs() {
    //Событие для кнопки
    $("#sensorCalibration").on('click', function(){ startCalibration(); });
    $("#clearCalibration").on('click', function(){ clearCalibration(); });
    
    $("#applyMinMax").on('click', function(){ applyMinMax(); });
    // Инициализация Select и Number спиннера
    //$('select').selectpicker();
    $("input[type='number']").inputSpinner();

    //toggle `popup` / `inline` mode
    $.fn.editable.defaults.mode = 'popup';     
    var minMaxConfig = {
      mode: 'popup',
      type: 'number',
      step: '0.01',
      min: '0.00',
      max: '24',
      inputclass: 'table-input',
      container: 'body',
      emptytext: 'Пусто'
    }

    $('#ph_min').editable(minMaxConfig);
    $('#ph_max').editable(minMaxConfig);
    $('#ec_min').editable(minMaxConfig);
    $('#ec_max').editable(minMaxConfig);
};

function setHtmlValuesFromJson (jsonData) {
    $.each(jsonData, function(key, value){
      if(key == 'ph_min' || key == 'ph_max' || key == 'ec_min' || key == 'ec_max') {
        if(!isMinMaxParsed) $('#'+ key).editable('setValue', (value / 1000).toFixed(2));
      } else if (document.getElementById(key) != null) {
        document.getElementById(key).innerHTML=(value/100).toFixed(2);
      }
    });
    isMinMaxParsed = true;
}

  /* Функция парсинга массива JSON насосов и изменения элементов управления pumpState на странице */
  function parseCalibrations(arr){
    var stateArr = ["calibrationState1", "calibrationState2"];
    for (var i = 0; i < arr.length; i++) {
    var pump = parseInt(arr[i]);
      if(document.getElementById(stateArr[i]) != null) {
        document.getElementById(stateArr[i]).classList.remove('text-success');
        document.getElementById(stateArr[i]).classList.remove('text-danger');
        document.getElementById(stateArr[i]).classList.add(pump ? "text-success" : "text-danger");
      }
    }
  }

function loadValues(){
    var xh = new XMLHttpRequest();
    xh.onreadystatechange = function(){
      if (xh.readyState == 4){
        if(xh.status == 200) {
          try {
            var res = JSON.parse(xh.responseText);

            setHtmlValuesFromJson(res);
            parseCalibrations(res.calibStates);

            setTimeout(loadValues, reloadPeriod);
          } catch(e) { console.log(e); }
          } else running = false;
      }
    };
    xh.open("GET", "/sensors", true);
    xh.send(null);
};

function startCalibration() {
    var value = Number(document.getElementById('sensorValue').value * 1000);
    var server = "/sensors.html?sensor=" + Number(document.getElementById("sersorType").selectedIndex) + "&value=" + value;
    request = new XMLHttpRequest();
	  request.open("GET", server, true);
    request.send();
    main.showNotificationByCode(3, 'info');
}

function clearCalibration() {
  var server = "/sensors.html?clearCalib=" + Number(document.getElementById("sersorType").selectedIndex);
  request = new XMLHttpRequest();
  request.open("GET", server, true);
  request.send();
  main.showNotificationByCode(3, 'info');
}


function applyMinMax() {
    var ph_min = Number(document.getElementById('ph_min').innerHTML * 1000);
    var ph_max = Number(document.getElementById('ph_max').innerHTML * 1000);
    var ec_min = Number(document.getElementById('ec_min').innerHTML * 1000);
    var ec_max = Number(document.getElementById('ec_max').innerHTML * 1000);

    var server = "/sensors.html?ph_min=" + ph_min + "&ph_max=" + ph_max + "&ec_min=" + ec_min + "&ec_max=" + ec_max;
    request = new XMLHttpRequest();
	request.open("GET", server, true);
    request.send();
    main.showNotificationByCode(3, 'info');

    isMinMaxParsed = false;
}