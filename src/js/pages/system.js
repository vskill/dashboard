var blobSlice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice,
    input = document.getElementById('fileName'),
    running = false,
    ua = navigator.userAgent.toLowerCase();

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++){
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function FocusOnInput(name) {
    var element = document.getElementById(name);
    element.focus();
    setTimeout(function () { element.focus(); }, 1);
}

function SetBlink(name) {
    document.getElementById(name).classList.add("blink");;
}

$(document).ready(function() {
    setTimeout(getSystem, 300);
    setTimeout(bindInputs, 1000);
    setTimeout(getNetwork, 1600);
    //setTimeout(getUpdateState, 2000);
});

var maxUpdateTime = 240000;

function handler(e){
    e.stopPropagation();
    e.preventDefault();
}

function move() {
    var elem = document.getElementById("progressBar"); 
    var width = 1;
    var id = setInterval(frame, maxUpdateTime / 100);
    function frame() {
      if (width >= 100) {
        clearInterval(id);
      } else {
        width++; 
        elem.style.width = width + '%'; 
      }
    }
  }

function updateFromServer() {
    var xh = new XMLHttpRequest();
    xh.onreadystatechange = function(){
      if (xh.readyState == 4){
        if(xh.status == 200) {
          try {
            var res = JSON.parse(xh.responseText);
            startUpdate(res.code, res.info);
            //updateCode = res.code;
            //updateInfo = res.info;
          } catch(e) { console.log(e); }
        }
      }
    };
    xh.open("GET", "/updatepossible", true);
    xh.send(null);
}

function startUpdate(updateCode, versionInfo) {
    if(versionInfo == undefined) versionInfo = 'Нет информации об обновлении</br>';
    if(updateCode == 0) {
        swal({
            type: 'success',
            title: 'Обновление',
            html:'<div class="row text-center">' +
                     '<label>Ваша система находится в актуальном состоянии и не нуждается в обновлении</label>' + 
                 '</div>',
            showCancelButton: false,
            showConfirmButton: true,
            confirmButtonClass: 'btn btn-success btn-round',
            buttonsStyling: false
        }).catch(swal.noop);
    } else if (updateCode == 4) {
        swal({
            type: 'error',
            title: 'Обновление',
            html:'<div class="row text-center">' +
                     '<label>Не удается соедениться с сервером обновления. Проверьте настройки сети.</label>' + 
                 '</div>',
            showCancelButton: false,
            showConfirmButton: true,
            confirmButtonClass: 'btn btn-success btn-round',
            buttonsStyling: false
        }).catch(swal.noop);
    } else if (updateCode > 0) {
        swal({
            title: 'Обновление',
            html:'<div class="row text-center">' +
                    '<label><u>Доступна новая версия.</u></br></br><i class=\"text-left\">' + versionInfo + '</i></br>Перед обновлением устройства убедитесь в стабильности интернет соединения и наличии питания на устройстве! <i>Обновление займет около 5 минут.</i></label>' + 
                '</div>',
            showCancelButton: true,
            showConfirmButton: true,
            confirmButtonClass: 'btn btn-success btn-round',
            cancelButtonClass: 'btn btn-danger btn-round',
            buttonsStyling: false
        }).then(function(result) {
            swal({
                title: 'Обновление',
                html:'<div class="row">' +
                        '<h5>Идет обновление устройства! Не выключайте питание!</h5>' + 
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-md-12">' + 
                            '<div id="updateProgressBar">' +
                                '<div id="progressBar"></div>' + 
                            '</div>'+
                        '</div>' + 
                    '</div>',
                showCancelButton: false,
                showConfirmButton: false,
                confirmButtonClass: 'btn btn-success btn-round',
                cancelButtonClass: 'btn btn-danger btn-round',
                buttonsStyling: false
            }).catch(swal.noop);
            move();
            document.addEventListener("click",handler,true);
            new Promise(function (resolve) {
                request = new XMLHttpRequest();
                request.open("GET", "/system.html?httpupdate=1", true);
                request.send();
                setTimeout(function () {
                    resolve(1);
                }, updateTime)
            }).then(function(result) {
                document.removeEventListener("click",handler,true);
                swal({
                    type: 'success',
                    html: '<h5>Обновление успешно</h5>',
                    confirmButtonClass: 'btn btn-success btn-round',
                    buttonsStyling: false
                }).catch(swal.noop)
            });
        });
    }
    // if(serverVersion != userVersion) {
    //     request.open("GET", "/system.html?httpupdate=1", true);
    //     request.send();
    //     main.showNotificationByCode(3, 'info');
    // }
}

var notLocked = true;
$.fn.animateHighlight = function(highlightColor, duration) {
    var highlightBg = highlightColor || "#FFFF9C";
    var animateMs = duration || 1500;
    var originalBg = this.css("backgroundColor");
    if (notLocked) {
        notLocked = false;
        this.stop().css("background-color", highlightBg)
            .animate({backgroundColor: originalBg}, animateMs);
        setTimeout( function() { notLocked = true; }, animateMs);
    }
};

function bindInputs() {
    if ($('.slider').length != 0) { main.initSliders(); }

    $("#mac").on('click', function(){ copyToClipboard("#mac"); } );
    $("#updateHTTPButton").on('click', function(){ updateFromServer(); });

    $("#saveGraph").on('click', function(){ applyGraphSettings(); });
    $("#clearGraph").on('click', function(){ clearGraph(); });
 
    $("#saveCfgBtn").on('click', function(){ saveCustomConfig(); });
    $("#loadCfgBtn").on('click', function(){ loadCustomConfig(); });

    getCustomConfigNames("#cfgList");
    $("#cfgList").change(function(){
        var selectedText =  $("#cfgList option:selected").text();
        $("#cfgName").val(selectedText.substring(5, selectedText.length));
    });

    $("#applyTime").on('click', function(){ applyTimeSettings(); });

    $("#ipaddr").on('click', function(){ window.open("http://" + document.getElementById("ipaddr").innerHTML); });

    $("#phctrl").change(function () { sendAutomaticControl(0, +this.checked); });
    $("#ecctrl").change(function () { sendAutomaticControl(1, +this.checked); });
    $("#addpowerctrl").change(function () { sendAutomaticControl(2, +this.checked); });
    $("#phwaterlevelctrl").change(function () { sendAutomaticControl(3, +this.checked); });

    var focus = GetURLParameter('focus');
    if(focus=="graph") {
        $("#graphCard").fadeTo(400, 0.1).fadeTo(400, 1.0).fadeTo(400, 0.1).fadeTo(400, 1.0).fadeTo(400, 0.1).fadeTo(400, 1.0);
        setTimeout(FocusOnInput('updgraphevery'),200);
        // $('.blink').each(function() {
        //     var elem = $(this);
        //     elem.fadeOut(100)
        //         .fadeIn(100)
        //         .fadeOut(100)
        //         .fadeIn(100)
        //         .fadeOut(100)
        //         .fadeIn(100);
        // });
        // var times = 1;
        // var blinkInterval = setInterval(function(){
        //     setTimeout(function(){
        //         $('#graphCard').css('background-color','#FFFFFF');
        //     }, 250);
        //     $('#graphCard').css('background-color','#51bcda');
        //     if(times++ === 3) {clearInterval(blinkInterval);}
        // }, 500);
        // setTimeout(FocusOnInput('graphCard'),200);
    }
}

function sendAutomaticControl (code, state) {
    //if(state === "undifined") return;
    var element = 0;
    switch (code) {
        case 0:
            element = "phcontrol";
            break;
        case 1:
            element = "eccontrol";
            break;
        case 2:
            element = "apcontrol";
            break;
        case 3:
            element = "waterlevelcontrol";
            break;
    }
    if(element != 0) {
        var server = "/system.html?" + element + "=" + state;
        request = new XMLHttpRequest();
        request.open("GET", server, true);
        request.send();
    }
}

function submitFrm(frm) {
    if (document.getElementById("wwwauth").checked) {
        if ((document.getElementById("wwwuser").value != "") && (document.getElementById("wwwpass").value != "")) {
            //document.getElementById("submitResult").innerHTML = "Auth updated";
            console.log("User: " + document.getElementById("wwwuser").value);
            console.log("Pass: " + document.getElementById("wwwpass").value);
            console.log("Enable: " + document.getElementById("wwwauth").checked);
            frm.submit();
        } else {
            document.getElementById("submitResult").innerHTML = "User and password must be filled";
        }
    } else {
        //document.getElementById("wwwuser").value = "";
        document.getElementById("wwwpass").value = "";
        //document.getElementById("submitResult").innerHTML = "Auth disabled";
        console.log("User: " + document.getElementById("wwwuser").value);
        console.log("Pass: " + document.getElementById("wwwpass").value);
        console.log("Enable: " + document.getElementById("wwwauth").checked);
        frm.submit();
        
    }
}

function restartESP() {
    setValues("/admin/restart");
}

function getUpdateState() {
    setValues("/update/updatepossible");
    setTimeout(function () {
    if (document.getElementById("remupd").innerHTML == "OK") {
       console.warn("ERROR = OK");
       document.getElementById("fileName").disabled = false;
       document.getElementById("updateButton").disabled = false;
       document.getElementById("remupdResult").innerHTML = "";
       }
    }, 2000);
}
function getMainSettings() {
    setValues("/admin/settings");
}

function summd5() {
    if (running) { return; }

    if (!input.files.length) {
        console.error('Please select a file');
        return;
    }
    var fileReader = new FileReader(),
        file = input.files[0],
        time;

    fileReader.onload = function (e) {
        running = false;
        if (file.size != e.target.result.length) {
            console.error('ERROR:Browser reported success but could not read the file until the end');
        } else {
            md5hash = SparkMD5.hashBinary(e.target.result);
            console.info('Finished loading!');
            console.info('Computed hash: ' + md5hash); // compute hash
            console.info('Total time: ' + (new Date().getTime() - time) + 'ms');
            document.getElementById('md5row').hidden = false;
            document.getElementById('clientHash').innerHTML = md5hash;
            document.getElementById('fileSize').innerHTML = file.size;
            setValues('/setmd5?md5=' + md5hash + '&size=' + file.size);
        }
    };
    fileReader.onerror = function () {
        running = false;
        console.error('ERROR: FileReader onerror was triggered, maybe the browser aborted due to high memory usage');
    };
    running = true;
    console.info('Starting normal test (' + file.name + ')');
    time = new Date().getTime();
    fileReader.readAsBinaryString(file);
}

/*
Блок Сетевые настройки
*/
function securityStr(security) {
    switch(security) {
      case 7:
        return 'Открыта';
      case 5:
        return "WEP <i class='nc-icon icon-lock'></i>";
      case 2:
        return "WPA <i class='nc-icon icon-lock'></i>";
      case 4:
        return "WPA2 <i class='nc-icon icon-lock'></i>";
      case 8:
        return "WPA/WPA2 <i class='nc-icon icon-lock'></i>";
    }
  }
  

function wifiScan(res) {
    var array;

    if (!res || (res.target.responseText == '[]')) {
        setTimeout(function () { getNetwork(); }, 5000);
        return;
    }
    array = JSON.parse(res.target.responseText);
    array.sort(function (a, b) { return a.rssi - b.rssi });
    array.reverse();
    document.getElementById("numNets").innerHTML = array.length;
    var table = document.getElementById("networks");
    table.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        var row = document.createElement("tr");
        row.innerHTML = "<td><a href='javascript:selssid(\"" + array[i].ssid + "\")'>" + array[i].ssid + "</td><td>" + array[i].channel + "</td><td>" + securityStr(array[i].secure) + "</td><td>" + getSignalPower(array[i].rssi) + "</td>";
        table.appendChild(row);
    }
}

// function getValues() {
//     setValues("/admin/values");
// }

function getNetwork() {
    request = new XMLHttpRequest();
    if (request) {
        request.open("GET", "/scan", true);
        request.addEventListener("load", wifiScan)
        request.send();
    }
}


function getFormattedDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear().toString().slice(2);
    return day + '-' + month + '-' + year;
}

function getSystem() {
    $("#ipaddress").hide();
    setValues("/admin/system");
    //var dateTime = setValues("/system");
    setTimeout(function() {
        if($("#ipaddr").html() != "N/A") {
            $("#ipaddress").show();
        }
    }, 500);
}

var getConnectionModalState = function() {
    var xh = new XMLHttpRequest();
    xh.onreadystatechange = function(){
      if (xh.readyState == 4){
        if(xh.status == 200) {
          try {
            var res = JSON.parse(xh.responseText);
            return [res.ssid, res.state];
          } catch(e) { console.log(e); }
        }
      }
    };
    xh.open("GET", "/admin/connectionmodalstate", true);
    xh.send(null);
}

function showSwal(type, ssid) {
    if (type == 'input-field') {
        swal({
            title: 'Подключение к точке WiFi',
            html:'<div class="row">' +
                    '<label class="col-md-4 col-form-label">SSID:</label>' + 
                      '<div class="col-md-8">' + 
                        '<div class="form-group">' + 
                          '<input type="text" class="form-control" id="mSsid" value="' + ssid + '"/>' + 
                        '</div>' + 
                      '</div>' + 
                '</div>' +
                '<div class="row">' +
                    '<label class="col-md-4 col-form-label">Пароль:</label>' + 
                      '<div class="col-md-8">' + 
                        '<div class="form-group">' + 
                          '<input type="password" class="form-control" id="mPassword"/>' + 
                        '</div>' + 
                      '</div>' + 
                '</div>',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success btn-round',
            cancelButtonClass: 'btn btn-danger btn-round',
            buttonsStyling: false
        }).then(function(result) {
            sendWiFiSettings($('#mSsid').val(), $('#mPassword').val());
        }).then(function(result) {
            modalWiFiResult("SAVED", $('#mSsid').val());
            restartESP();
        }).catch(swal.noop);
        // }).then(setTimeout(function(result) {
        //         var state = getConnectionModalState();
        //         modalWiFiResult(state[0], state[1]);
        //     } ,2200)).catch(swal.noop);
    }
}

function getSignalPower(rssi) {
    var signal;
    var bars = "<div class='first-bar bar'></div><div class='second-bar bar'></div><div class='third-bar bar'></div><div class='fourth-bar bar'></div><div class='fifth-bar bar'></div>"; 
    if(rssi >= -67) {                    // amazing
      signal = "<div class='signal-bars mt1 sizing-box good five-bars'>";
    }
    else if(rssi < -67 && rssi >= -70) { // good
      signal = "<div class='signal-bars mt1 sizing-box good four-bars'>";
    }
    else if(rssi < -70 && rssi >= -80) { // ok
      signal = "<div class='signal-bars mt1 sizing-box ok three-bars'>";
    }
    else if(rssi < -80 && rssi >= -90) { // not good
      signal = "<div class='signal-bars mt1 sizing-box bad two-bars'>";
    }
    else if(rssi < -90) {               // bad
      signal = "<div class='signal-bars mt1 sizing-box bad one-bar'>";
    }
    return (signal + bars + "</div>");
  }

function modalWiFiResult(state, ssid) {
    if(state == "SAVED") {
        swal({
            type: 'success',
            html: 'Изменения сохранены. </br><strong>Дождитесь перезагрузки устройства!</strong>',
            confirmButtonClass: 'btn btn-success btn-round',
            buttonsStyling: false
        }).catch(swal.noop)
    } else if(state == "CONNECTED") {
        swal({
            type: 'success',
            html: 'Вы присоеденены к: <strong>' + ssid + '</strong>',
            confirmButtonClass: 'btn btn-success btn-round',
            buttonsStyling: false
        }).catch(swal.noop)
    } else {
        swal({
            type: 'danger',
            html: 'Не удалось присоедениться к: <strong>' + ssid + '</strong></br>Проверьте правильность вводимых данных или попробуйте позже.',
            confirmButtonClass: 'btn btn-success btn-round',
            buttonsStyling: false
        }).catch(swal.noop)
    }
}
function selssid(value) {
    //document.getElementById("ssid").value = value;
    showSwal('input-field', value);
}

function getNTPState() {
    setValues("/admin/ntpvalues");
}

function sendWiFiSettings(ssid, pwd) {
    var server = "/system.html?ssid=" + ssid + "&password=" + pwd;
    request = new XMLHttpRequest();
	request.open("GET", server, true);
    request.send();
}
function applyTimeSettings() {
    var u_time = moment.utc(document.getElementById('datetimepicker').value, 'MM-DD-YYYY HH:mm:ss').unix();
    //var server = "/system.html?ntpenabled=" + Number(document.getElementById('ntpenabled').checked) + "&u_time=" + u_time + "&ntpserver=" + document.getElementById('ntpserver').value + "&update=" + document.getElementById('update').value + "&tz=" + document.getElementById('tz').value + "&dst=" + document.getElementById('dst').value;
    var server = "/system.html?ntpenabled=" + Number(document.getElementById('ntpenabled').checked)  + "&tz=" + document.getElementById('tz').value + "&u_time=" + u_time;
    request = new XMLHttpRequest();
	request.open("GET", server, true);
    request.send();
    main.showNotificationByCode(3, 'info');
}

function applyGraphSettings () {
    var server = "/system.html?graphmax=" + document.getElementById('graphmaxcount').value + "&graphupd=" + document.getElementById('updgraphevery').value * 1000;
    request = new XMLHttpRequest();
	request.open("GET", server, true);
    request.send();
    main.showNotificationByCode(3, 'info');
}

function clearGraph () {
    request = new XMLHttpRequest();
	request.open("GET", "/system.html?cleargraph=1", true);
    request.send();
    main.showNotificationByCode(4, 'info');
}


function saveCustomConfig() {
    var parameters = "/system.html?scfg_id=" + $("#cfgList").prop("selectedIndex") + "&scfg_name=" + $("#cfgName").val();
    request = new XMLHttpRequest();
	request.open("GET", parameters, true);
    request.send();
    main.showNotificationByCode(4, 'info');
}

function loadCustomConfig() {
    var parameters = "/system.html?lcfg_id=" + $("#cfgList").prop("selectedIndex");
    request = new XMLHttpRequest();
	request.open("GET", parameters, true);
    request.send();
    main.showNotificationByCode(4, 'info');
}

function getCustomConfigNames(input) {
    var xh = new XMLHttpRequest();
    xh.onreadystatechange = function(){
      if (xh.readyState == 4){
        if(xh.status == 200) {
          try {
            var res = JSON.parse(xh.responseText);
            var $el = $(input);
            $el.empty(); // remove old options
            var result = res.configs;
            for(var index in result) {
                var configName = result[index] == null ?  "Пусто" : result[index];
                var itemText = "№" + (parseInt(index) + 1) + " - "  + configName;
                $el.append($("<option></option>")
                    .attr("value", index).text(itemText));
            };
            $("#cfgList").prop('selectedIndex', 0);
            var selectedText =  $("#cfgList option:selected").text();
            $("#cfgName").val(selectedText.substring(5, selectedText.length));
          } catch(e) { console.log(e); }
        }
      }
    };
    xh.open("GET", "/userconfigs", true);
    xh.send(null);
}

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);

    /* Select the text field */
    $temp.val($(element).text()).select();
    
    /* Copy the text inside the text field */
    document.execCommand("copy");
    $temp.remove();
    main.showNotificationByCode(5, 'primary');
}