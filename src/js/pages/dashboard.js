  // var websock = null;
  // var wsUri = "ws://" + window.location.hostname + "/ws";
  var temp;
  var reloadPeriod = 1000;
  var reloadGraphPeriod = 60000;
  var running = false;
  /*Минималистичные графики*/
  var chartPH;
  var chartEC;
  var chartT;

  // function startSocket(){
  //   //ws = new WebSocket('ws://'+document.location.host+'/ws',['arduino']);
  //   ws = new WebSocket('ws://192.168.4.1/ws',['arduino']);
  //   ws.binaryType = "arraybuffer";
  //   ws.onopen = function(e){
  //     console.log("WS Connected");
  //   };
  //   ws.onclose = function(e){
  //     console.log("WS Disconnected");
  //   };
  //   ws.onerror = function(e){
  //     console.log("WS Error:", e);
  //   };
  //   ws.onmessage = function(e){
  //     var msg = "";
  //     if(e.data instanceof ArrayBuffer){
  //       msg = "BIN:";
  //       var bytes = new Uint8Array(e.data);
  //       for (var i = 0; i < bytes.length; i++) {
  //         msg += String.fromCharCode(bytes[i]);
  //       }
  //     } else {
  //       msg = "TXT:"+e.data;
  //     }
  //     console.log("WS Msg:", msg);
  //   };
  // }
  $(document).ready(function() {
    /* DEBUG ONLY */
    // setTimeout(loadValues, 500);
    // setTimeout(minimalGraphs(), 2000);
    /* DEBUG ONLY */

    setTimeout(checkFirstLaunch, 200);  // Проверка на первый запуск
    setTimeout(startEvents, 100);
    setTimeout(bindInputs, 10);

    setTimeout(GetChartData, 2000);
    setInterval(function(){ GetChartData(); }, reloadGraphPeriod);
  });

  function checkFirstLaunch() {
    $.ajax({
      type: "GET",
      url: "firstlaunch",
      dataType: "text",
      success: function (data) {
        if(data == "1") {
          $("#welcomeModal").modal();
        }
      }
    });
  };

var minimalGraphs = (function() { 
  // Чтобы исполнялась только один раз
  var executed = false;
  return function() {
    if (!executed) {
      executed = true;

      var labelsPHleg = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"];
      var dataPHleg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
      var labelsECleg = ["1","2","3","4","5","6","7","8","9","10","1","2","3","4","5","6","7","8","9","10"];
      var dataECleg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
      var labelsTleg = ["1","2","3","4","5","6","7","8","9","10","1","2","3","4","5","6","7","8","9","10"];
      var dataTleg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

      var canvasPH = $("#graphPH");
      var canvasEC = $("#graphEC");
      var canvasT = $("#graphT");

      var dataPH = {
        labels : labelsPHleg,
        datasets: [{ label:'PH dataset', data: dataPHleg, lineTension : 0, borderColor : "#fbc658", fill: false }]
      };

      var dataEC = {
        labels : labelsECleg,
        datasets: [{ data: dataECleg, lineTension : 0, borderColor : "#6bd098", fill: false }]
      };

      var dataT = {
        labels : labelsTleg,
        datasets: [{ data: dataTleg, lineTension : 0, borderColor : "#ef8157", fill: false }]
      };

      chartPH = new Chart(canvasPH, {
        type: "line",
        data: dataPH,
        options: {
          responsive: true,
          scales:{ xAxes: [{ display: false }],
                  yAxes: [{ display: false, ticks: { beginAtZero:true }}] },
          legend: { display: false },
          tooltips: { enabled: false },
          elements: { point:{ radius: 0 } },
          maintainAspectRatio: false
        }
      });

      chartEC = new Chart(canvasEC, {
        type: "line",
        data: dataEC,
        options: {
          scales:{ xAxes: [{ display: false }],
                  yAxes: [{ display: false, ticks: { beginAtZero:true }}] },
          legend: { display: false },
          tooltips: { enabled: false },
          elements: { point:{ radius: 0 } },
          maintainAspectRatio: false,
          responsive: true
        }
      });

      chartT = new Chart(canvasT, {
        type: "line",
        data: dataT,
        options: {
          scales:{ xAxes: [{ display: false }],
                  yAxes: [{ display: false, ticks: { beginAtZero:true }}] },
          legend: { display: false },
          tooltips: { enabled: false },
          elements: { point:{ radius: 0 } },
          maintainAspectRatio: false,
          responsive: true
        }
      });
      setInterval(function(){ updateCharts();} , 1000);
    }
  };
})();

function updateCharts() {
  moveChart(chartPH, parseInt(parseFloat(document.getElementById("ph").innerHTML) * 100));
  moveChart(chartEC, parseInt(parseFloat(document.getElementById("ec").innerHTML) * 100));
  moveChart(chartT, parseInt(parseFloat(document.getElementById("temp").innerHTML) * 100));
}

function moveChart(chart, newData) {
  chart.data.labels.splice(0, 1); // remove first label
  chart.data.datasets.forEach(function(dataset) {
    dataset.data.splice(0, 1); // remove first data point
  });
  chart.update();

  chart.data.labels.push('new'); // add new label at end
  chart.data.datasets.forEach(function(dataset) {
    dataset.data.push(getValueCalculated(newData));
  });
  chart.update();
  //Автомасштаб
  var Min = getValueCalculated(Math.min.apply(this, chart.data.datasets[0].data));
  var Max = getValueCalculated(Math.max.apply(this, chart.data.datasets[0].data));
  chart.options.scales.yAxes[0].ticks.min = Min - 5;
  chart.options.scales.yAxes[0].ticks.max = Max + 5;
  chart.update();
}

function getValueCalculated(value){
  if(value < 100) return Math.floor(value);
  if(value < 1000) return Math.floor(value / 10);
  if(value < 10000) return Math.floor(value / 100);
  if(value < 100000) return Math.floor(value / 1000);
  if(value < 1000000) return Math.floor(value / 10000);
  if(value < 10000000) return Math.floor(value / 100000);
  if(value < 100000000) return Math.floor(value / 1000000);
  if(value < 1000000000) return Math.floor(value / 10000000);
  if(value < 10000000000) return Math.floor(value / 100000000);
}

function bindInputs(){
  $(".btn-update").click(function() {
    GetChartData();
    if (  $( this ).css( "transform" ) == 'none' ){
        $(this).css("transform","rotate(360deg)");
    } else {
        $(this).css("transform","" );
    }
  });
  $(".newMode").click(function() {
    request = new XMLHttpRequest();
	  request.open("GET", "/system.html?letter=0", true);
    request.send();
  }
  )
}

function startEvents() {
  if (!!window.EventSource) {
    var evs = new EventSource('/events');
    evs.onopen = function (evt) {
        console.log("Event source connected");
    };
    evs.onerror = function (evt) {
        if (evt.target.readyState != EventSource.OPEN) {
            console.log("Event source disconnected. Error: " + evt.data);
        }
    };
    evs.onmessage = function (evt) {
        console.log("Event " + evt.data);
    };
    evs.addEventListener('evsData', function (evt) {
        var jsonData = JSON.parse(evt.data);
        //Основные парамеры
        setHtmlValuesFromJson (jsonData);
        parseVisualSensors(jsonData.pumps, jsonData.powers, jsonData.gerkons);
        //Инициализация мини-графиков
        minimalGraphs();
        //Время обновлений      
        updateTime('reloadTime',(reloadPeriod/1000).toFixed(1));
        updateTime('reloadTimeGraph',(reloadGraphPeriod/60000).toFixed(0));
    }, false);
  }
};


  function updateTime(idName, Value) {  
    var items = document.getElementsByName(idName);
    for (var i=0; i < items.length; i++) {
      items[i].innerHTML = Value;
    }
  }

  function setHtmlValuesFromJson (jsonData) {
      $.each(jsonData, function(key, value){
        //if(key == 'upd_time') { 
          $(".upd_time").html((reloadPeriod/1000).toFixed(0));
        //}
        if(key == 'graph_val') { $(".graph_val").html(value);}
        if(key == 'graph_int') {document.getElementById(key).innerHTML=(value/1000).toFixed(0);}
        else if(key == 'time' || key == 'rtime' || key == 'graph_val') {
          document.getElementById(key).innerHTML=value;
        } else if (document.getElementById(key) != null) {
          document.getElementById(key).innerHTML=(value/1000).toFixed(2);
        }
      });
  }

  /* Функция парсинга массива JSON насосов и изменения элементов управления pumpState на странице */
  function parseVisualSensors(pumpsArray, powersArray, gerconArray){
    var parseArray = [
      ["pumpState1", "pumpState2", "pumpState3", "pumpState4", "pumpState5"],
      ["powerState1", "powerState2"],
      ["gerconState1", "gerconState2"]
    ];
    for (var i = 0; i < parseArray.length; i++) {
      for (var b = 0; b < parseArray[i].length; b++) {
        
        if(i==0) var node = parseInt(pumpsArray[b]);
        if(i==1) var node = parseInt(powersArray[b]);
        if(i==2) var node = parseInt(gerconArray[b]);

        if(document.getElementById(parseArray[i][b]) != null) {
          document.getElementById(parseArray[i][b]).classList.remove('text-success');
          document.getElementById(parseArray[i][b]).classList.remove('text-danger');
          document.getElementById(parseArray[i][b]).classList.add(node ? "text-success" : "text-danger");
        }
      }
    }
  }

  /* Функция парсинга массива JSON насосов и изменения элементов управления pumpState на странице */
  function parsePowers(powersArray){
    var powerStateArr = ["powerState1", "powerState2"];
    for (var i = 0; i < powersArray.length; i++) {
    var power = parseInt(powersArray[i]);
    if(document.getElementById(powerStateArr[i]) != null) {
      document.getElementById(powerStateArr[i]).classList.remove('text-success');
      document.getElementById(powerStateArr[i]).classList.remove('text-danger');
      document.getElementById(powerStateArr[i]).classList.add(power ? "text-success" : "text-danger");
    }
    }
  }


    function loadValues(){
      var xh = new XMLHttpRequest();
      xh.onreadystatechange = function(){
        if (xh.readyState == 4){
          if(xh.status == 200) {
            try {
              var res = JSON.parse(xh.responseText);

              setHtmlValuesFromJson (res);
              parseVisualSensors(res.pumps, res.powers, res.gerkons);
              
              updateTime('reloadTime',(reloadPeriod/1000).toFixed(1));
              updateTime('reloadTimeGraph',(reloadGraphPeriod/60000).toFixed(0));
              
              //if(running) 
              setTimeout(loadValues, reloadPeriod);
            } catch(e) { console.log(e); }
            } else running = false;
        }
      };
      xh.open("GET", "/all", true);
      xh.send(null);
    };

/* Функция обновления и настроек графика из graph.json */
function GetChartData() {
  if(window.chart && window.chart !== null){
    window.chart.destroy();
  }
  // Add a helper to format timestamp data
  Date.prototype.formatHHMM = function() {
    return this.getHours() + ":" +  this.getMinutes();
  }

  var jsonData = $.ajax({
    url: 'graph',
    dataType: 'json',
  }).done(function (results) {
  // Split timestamp and data into separate arrays
  var labels = [], dataT = [], dataH = [], dataCO2 = [], dataPH = [], dataEC = [];
  results["p"].forEach(function(packet) {
    //console.log(packet);
    //labels.push(new Date(moment(packet.Time, 'HH:mm:ss DD-MM-YYYY')).toTimeString().split(' ')[0]);
    //labels.push(new Date(moment(packet.Time, 'HH:mm:ss DD-MM-YYYY')).formatHHMM());
    if(moment(packet.Tm, 'HH:mm:ss DD-MM-YYYY').isValid()) {
      labels.push(new Date(moment(packet.Tm, 'HH:mm:ss DD-MM-YYYY')));
      dataT.push(parseInt(packet.T)/1000);
      dataPH.push(parseInt(packet.P)/1000);
      dataEC.push(parseInt(packet.E)/1000);
      // dataH.push(parseInt(packet.H));
      // dataCO2.push(parseInt(packet.CO2));
    }
  });
  var tempData = {
    labels : labels,
    datasets : [{
      label       : "T",
      borderColor : "#ef8157",
      fill        : false,
      lineTension : 0,
      data        : dataT
    },
    // {
    //   label       : "H",
    //   borderColor : "#8e5ea2",
    //   fill        : false,
    //   lineTension : 0,
    //   data        : dataH
    // },
    // {
    //   label       : "CO2",
    //   borderColor : "#3cba9f",
    //   fill        : false,
    //   lineTension : 0,
    //   data        : dataCO2
    // },
    {
      label       : "PH",
      borderColor : "#fbc658",
      fill        : false,
      lineTension : 0,
      data        : dataPH
    },
    {
      label       : "EC",
      borderColor : "#6bd098",
      fill        : false,
      lineTension : 0,
      data        : dataEC
    }]
  };

  // Get the context of the canvas element we want to select
  var ctx = document.getElementById("graphParameters").getContext("2d");

  // Instantiate a new chart
  window.chart = new Chart(ctx , {
    type: "line",
    data: tempData, 
    options: {
      tooltips: {
        enabled: true,
        intersect: false,
        position: 'nearest',
        mode: 'index',
        titleFontSize: 16
      },
      pan: {
        enabled: true,
        mode: 'xy'
      },
      zoom: {
        enabled: true,
        mode: 'xy'
      },
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          type: 'linear',
          ticks: {
            beginAtZero: true
          }
        }],
        xAxes: [{
          type: 'time',
          time: {
            unit: 'minute',
            tooltipFormat: 'DD.MM.YYYY HH:mm:ss',
            displayFormats: {
              minute: 'HH:mm'
            }
          }
        }]
      },
    },
    responsive: true
  });
});
}
    	