function microAjax(B,A){
    this.bindFunction=function(E,D) {
        return function() {
            return E.apply(D,[D]);
        };
    };
    this.stateChange=function(D) {
        if (this.request.readyState == 4) {
            this.callbackFunction(this.request.responseText);
        }
    };
    this.getRequest = function () {
        if (window.ActiveXObject) {
            return new ActiveXObject("Microsoft.XMLHTTP");
        } else {
            if (window.XMLHttpRequest) {
                return new XMLHttpRequest();
            }
        } return false;
    };
    this.postBody = (arguments[2] || "");
    this.callbackFunction = A;
    this.url = B;
    this.request = this.getRequest();
    if (this.request) {
        var C = this.request;
        C.onreadystatechange = this.bindFunction(this.stateChange, this);
        if (this.postBody !== "") {
            C.open("POST", B, true);
            C.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            C.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            C.setRequestHeader("Connection", "close");
        } else {
            C.open("GET", B, true);
        }
        C.send(this.postBody);
    }
}
// const waitFor = (ms) => new Promise(r => setTimeout(r, ms));

// async function asyncForEach(array, callback) {
//     for (let index = 0; index < array.length; index++) {
//       await callback(array[index], index, array);
//     }
// }

function setValues(url)
{
	microAjax(url, function (res) {
	    res.split(String.fromCharCode(10)).forEach(
            function (entry) {
                var fields = entry.split("|");
                if(fields[0] == "nowtime") {
                    $('#datetimepicker').datetimepicker({
                        icons: {
                        time: "nc-icon icon-clock-1",
                        date: "nc-icon icon-calendar",
                        up: "nc-icon icon-up-open",
                        down: "nc-icon icon-down-open",
                        previous: 'nc-icon icon-left-open',
                        next: 'nc-icon icon-right-open',
                        today: 'nc-icon icon-desktop',
                        clear: 'nc-icon icon-trash-empty',
                        close: 'nc-icon icon-cancell'
                        },
                        locale: 'ru',
                        format: 'DD.MM.YYYY HH:mm:ss',
                    });
                    // Неправильный формат. Ошибка в datetimepicker 'MM-DD-YYYY HH:mm:ss' должен быть 'DD-MM-YYYY HH:mm:ss'
                    var momentString = moment(fields[1], 'HH:mm:ss DD-MM-YYYY').locale('ru').format('MM-DD-YYYY HH:mm:ss'); 
                    $('#datetimepicker').data("DateTimePicker").date(new Date(momentString));
                }
                if(document.getElementById(fields[0]) != null) {
                    if(fields[0] == "updgraphevery") fields[1] = fields[1]/1000;
                    if(fields[2] == "input") {
                        document.getElementById(fields[0]).value = fields[1];
                    } else if(fields[2] == "div") {
                        document.getElementById(fields[0]).innerHTML  = fields[1];
                    } else if(fields[2] == "chk") {
                        document.getElementById(fields[0]).checked  = fields[1];
                    }
                    // } else if(fields[2] == "bschk") {
                    //     if(fields[1] == "true") {
                    //         $("#"+fields[0]).bootstrapSwitch('toggleState'); 
                    //     }
                    //}
                }
            }
        );
    });
}